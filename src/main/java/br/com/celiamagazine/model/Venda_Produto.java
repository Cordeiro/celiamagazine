package br.com.celiamagazine.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity
public class Venda_Produto extends AbstractEntity{

    private BigDecimal valorDesconto;
    @ManyToOne
    private Produto produto;
    private int quantidade;
    /*@ManyToOne
    private Venda venda;*/

    public Venda_Produto(){}

    public Venda_Produto(Produto produto, String valorDesconto, int quantidade) {
        this.produto = produto;
        this.valorDesconto = new Venda().formatValorString(valorDesconto);
        this.quantidade = quantidade;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public String toString() {
        return "Venda_Produto{" +
                "valorDesconto=" + valorDesconto +
                ", produto=" + produto +
                ", quantidade=" + quantidade +
                '}';
    }
}
