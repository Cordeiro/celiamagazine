package br.com.celiamagazine.model;

import br.com.celiamagazine.migrate.DBFReader;
import br.com.celiamagazine.migrate.JDBFException;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Fornecedor extends AbstractEntity {

	//@NotEmpty
	private String razaoSocial;
	private String contato;
	private String inscricaoEstadual;
	//@NotEmpty
	private String cnpj;
	private String email;
	private String telefone;
	private String celular;
	private String observacao;
	@OneToOne
	@NotNull
	@Valid
	private Endereco endereco;
	@OneToMany(mappedBy = "fornecedor")
	private List<Produto> produtos;

	
	/*REMOVER*/
	@Transient
	DBFReader dbfReader;
	
	public Fornecedor() {
//		this.endereco = new Endereco();
	}
	
	public Set<Fornecedor> lerDBF(String path) throws Exception{
		Set<Fornecedor> list = new HashSet<Fornecedor>();
		try {
			dbfReader = new DBFReader(path);
			/*for (int i = 0; i < dbfReader.getFieldCount(); i++)
				System.out.print(dbfReader.getField(i).getName() + "-"); // NOME DOS ATRIBUTOS*/ 
			
			for (; dbfReader.hasNextRecord();) 
				list.add(getDBF(dbfReader));
			
		}catch (JDBFException e) {
			e.printStackTrace();
			System.out.println("Erro! Arquivo '" + path + "'");
		}
		
		return list;
	}
	
	private Fornecedor getDBF(DBFReader dbf) throws Exception{
		Object aobj[];
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setEndereco(new Endereco());
		try {
			aobj = dbf.nextRecord(Charset.forName("ISO8859-1"));
			for (int i = 0;	i< aobj.length; i++) {
				switch (i) {
				case 0: // NOME
					fornecedor.setRazaoSocial(valide(aobj[i].toString()));
					break;
				case 1: // CONTATO
					fornecedor.setContato(valide(aobj[i].toString()));
					break;
				case 2: // IE
					fornecedor.setInscricaoEstadual(valide(aobj[i].toString()));
					break;
				case 3: // IM
					break;
				case 4: // CGC
					fornecedor.setCnpj(valide(valide(aobj[i].toString())));
					break;
				case 5: // ENDERE
					fornecedor.getEndereco().setLogradouro(valide(aobj[i].toString()));
					break;
				case 6: // COMPLE
					fornecedor.getEndereco().setBairro(valide(aobj[i].toString()));
					break;
				case 7: // CIDADE
					fornecedor.getEndereco().setCidade(valide(aobj[i].toString()));
					break;
				case 8: // ESTADO
					//TODO CONSERTAR ESTADO
//					fornecedor.getEndereco().setEstado(valide(aobj[i].toString()));
					break;
				case 9: // CEP
					fornecedor.getEndereco().setCep(valide(aobj[i].toString()));
					break;
				case 10: // FONE
					fornecedor.setTelefone(valide(aobj[i].toString()));
					break;
				case 11: // FAX
					break;
				case 12: // CELULAR
					break;
				case 13: // EMAIL
					fornecedor.setEmail(valide(aobj[i].toString()));
					break;
				case 14: // OBS
					fornecedor.setObservacao(valide(aobj[i].toString()));
					break;
				case 15: // ATIVO
					break;
				default:
					throw new Exception("ERRO! Limite ultrapassado");
				}
			}
		} catch (NullPointerException e) {
		} catch (JDBFException e) {
			e.printStackTrace();
		}
		return fornecedor;
	}
	
	private String valide(String aux) {
		if (aux.equals("") ) {
			return null;
		}
		return aux;
	}
	
	/*REMOVER*/
	
	

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}
	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String toString() {
		return "Fornecedor [razaoSocial=" + razaoSocial + ", cnpj=" + cnpj + ", id=" + id + "]";
	}
}