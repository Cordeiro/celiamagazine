package br.com.celiamagazine.model;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Venda_Restante extends AbstractEntity{

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataEmissao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataVencimento;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRecebimento;
    @NumberFormat(pattern = "###,##0.00")
    private BigDecimal valorTotal;
    @NumberFormat(pattern = "###,##0.00")
    private BigDecimal valorRecebido;
    @ManyToOne
    @JoinColumn(name="venda_id")
    private Venda venda;

    public Venda_Restante() { }

    public Venda_Restante(Venda venda, BigDecimal restante){
        this.venda = venda;
        this.dataEmissao = venda.getDataEmissao();
        this.dataVencimento = new Date();
        this.dataVencimento.setMonth(venda.getDataVencimento().getMonth() + 1);
        this.valorTotal = restante;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Date getDataRecebimento() {
        return dataRecebimento;
    }

    public void setDataRecebimento(Date dataRecebimento) {
        this.dataRecebimento = dataRecebimento;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getValorRecebido() {
        return valorRecebido;
    }

    public void setValorRecebido(BigDecimal valorRecebido) {
        this.valorRecebido = valorRecebido;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }
}
