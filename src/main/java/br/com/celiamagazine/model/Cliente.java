package br.com.celiamagazine.model;

import br.com.celiamagazine.migrate.Convert;
import br.com.celiamagazine.migrate.DBFReader;
import br.com.celiamagazine.migrate.JDBFException;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Cliente extends Usuario{

	public Cliente () {}

	
//	public Cliente() {
//		this.setEnderecos(new ArrayList<>()); 
//	}
	
	public Cliente(String nome, String cpf, String rg, String celular, String telefone, String email, String observacao,
			String imagem, Date dtCadastro, Date dtNascimento, String pai, String mae, String contato, String username,
			String password, Endereco endereco) {
		super(nome, cpf, rg, celular, telefone, email, observacao, imagem, dtCadastro, dtNascimento, pai, mae, contato,
				username, password, endereco);
	}

	public Cliente(String nome, String cpf, String rg, String telefone, String email, String observacao,
			Date dtCadastro, Date dtNascimento, String pai, String mae, String contato) {
		super(nome, cpf, rg, telefone, email, observacao, dtCadastro, dtNascimento, pai, mae, contato);
	}


	/*REMOVER*/
	@Transient
	DBFReader dbfReader;
	
	public Set<Cliente> lerDBF(String path) throws Exception {
		Set<Cliente> list = new HashSet<Cliente>();
		try {
			dbfReader = new DBFReader(path);
			/*
			 * for (int i = 0; i < dbfReader.getFieldCount(); i++)
			 * System.out.print(dbfReader.getField(i).getName() + "-"); // NOME DOS
			 * ATRIBUTOS
			 */

			for (;dbfReader.hasNextRecord();)
				list.add(getDBF(dbfReader));

			/*
			 * for (Cliente cliente : list) { System.out.println("\n"+cliente); }
			 */
		} catch (JDBFException e) {
			e.printStackTrace();
			System.out.println("Erro! Arquivo '" + path + "'");
		}

		return list;
	}

	public Cliente getDBF(DBFReader dbf) throws Exception {
		Object aobj[];
		Cliente cliente = new Cliente();
		Endereco end = new Endereco();
		try {
			aobj = dbf.nextRecord(Charset.forName("ISO8859-1"));
			for (int i = 0; i < aobj.length; i++) {
				switch (i) {
				case 0: // NOME
					cliente.setNome(valide(aobj[i].toString()));
					break;
				case 1: // CONTATO
					cliente.setContato(valide(aobj[i].toString()));
					break;
				case 2: // RG(IE)
					//cliente.setRg(valide(aobj[i].toString()));
					cliente.setRg("000.000-00");
					break;
				case 3: // (IM)
					break;
				case 4: // (CGC)
					//cliente.setCpf(valide(aobj[i].toString()));
					cliente.setCpf("000.000.000-00");
					break;
				case 5: // RUA
					end.setLogradouro(valide(aobj[i].toString()));
					break;
				case 6: // COMPLEMENTO
					end.setBairro(valide(aobj[i].toString()));
					break;
				case 7: // CIDADE
					end.setCidade(valide(aobj[i].toString()));
					break;
				case 8: // ESTADO
					//TODO CONSERTAR ESTADO
//					end.setEstado(valide(aobj[i].toString()));
					break;
				case 9: // CEP
					end.setCep(valide(aobj[i].toString()));
					break;
				case 10: // FONE
					cliente.setTelefone(valide(aobj[i].toString()));
					break;
				case 11: // FAX
					break;
				case 12: // EMAIL
					cliente.setEmail(valide(aobj[i].toString()));
					break;
				case 13: // OBSERVAÇÃO
					cliente.setObservacao(valide(aobj[i].toString()));
					break;
				case 14: // CELULAR
					break;
				case 15: // CREDITO
					break;
				case 16: // PAI
					cliente.setPai(valide(aobj[i].toString()));
					break;
				case 17: // MAE
					cliente.setMae(valide(aobj[i].toString()));
					break;
				case 18: // CONJUGE
					break;
				case 19: // NATURAL
					break;
				case 20: // PROFISSAO
					break;
				case 21: // CONVENIO
					break;
				case 22: // DATA NASCIMENTO
					cliente.setDtNascimento(new Convert().formatData(aobj[i].toString()));
					break;
				case 23: // CADASTRO DATA
					cliente.setDtCadastro(new Convert().formatData(aobj[i].toString()));
					break;
				case 24: // ULTIMA COMPRA DATA
					break;
				case 25: // PROXIMA DATA
					break;
				case 26: // CUSTO
					break;
				case 27: // COMPRA
					break;
				case 28: // ATIVO
					break;
				case 29: // MOSTRAR
					break;
				default:
					throw new Exception("ERRO! Limite ultrapassado");
				}
			}
		} catch (NullPointerException e) {
		} catch (JDBFException e) {
			e.printStackTrace();
		}
		cliente.setEndereco(end);
		return cliente;
	}
	
	private String valide(String aux) {
		if (aux.equals("") ) {
			return null;
		}
		return aux;
	}
	
	/*REMOVER*/
	
}
