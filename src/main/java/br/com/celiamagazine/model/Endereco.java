package br.com.celiamagazine.model;

import javax.persistence.Entity;
import javax.validation.constraints.Min;

@Entity
public class Endereco extends AbstractEntity{
	
	//@NotBlank
	private String logradouro;
	//TODO FALTA ADD
	//@NotBlank
	private String bairro;
	//@NotBlank
	private String cep;
	//@NotBlank
	private String cidade;
	private Estado estado;
	//@NotNull
	@Min(0)
	private int numero;
	//TODO FALTA ADD
	private String complemento;
	
	
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	@Override
	public String toString() {
		return "Endereco [logradouro=" + logradouro + ", bairro=" + bairro + ", cep=" + cep + ", cidade=" + cidade
				+ ", estado=" + estado + ", numero=" + numero + ", complemento=" + complemento + "]";
	}
}