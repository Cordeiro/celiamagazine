package br.com.celiamagazine.model;

public enum Cargo {
	ADMINISTRADOR, ATENDENTE,CAIXA, REDATOR;
}