package br.com.celiamagazine.model;

import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@Entity
public class Funcionario extends Usuario{
	
	private Cargo cargo;
	
	public Funcionario(String nome, String cpf, String rg, String celular, String telefone, String email,
			String observacao, String imagem, Date dtCadastro, Date dtNascimento, String pai, String mae,
			String contato, String username, String password, Endereco endereco, List<Role> roles) {
		super(nome, cpf, rg, celular, telefone, email, observacao, imagem, dtCadastro, dtNascimento, pai, mae, contato,
				username, password, endereco);
	}

	public Funcionario() {	}

	public Funcionario(Funcionario funcionario) {
		super(funcionario.getNome(), funcionario.getCpf(),funcionario.getRg(),funcionario.getCelular(),funcionario.getTelefone(),funcionario.getEmail(),funcionario.getObservacao(),funcionario.getImagem(),funcionario.getDtCadastro(),funcionario.getDtNascimento(),funcionario.getPai(),funcionario.getMae(),funcionario.getContato(),
				funcionario.getUsername(),funcionario.getPassword(),funcionario.getEndereco());
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	@Override
	public String toString() {
		return "Funcionario [cargo=" + cargo + "(),toString()=" + super.toString() + "]";
	}
}