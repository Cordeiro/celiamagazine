package br.com.celiamagazine.model;

import java.math.BigDecimal;
import java.util.Arrays;

public class ProdutoModelForm {

    private String[] nome;
    private String[] valorUnid;
    private int[] unidade;
    private String[] valorDesconto;
    private BigDecimal[] valorTotalV;

    public String[] getNome() {
        return nome;
    }

    public void setNome(String[] nome) {
        this.nome = nome;
    }

    public String[] getValorUnid() {
        return valorUnid;
    }

    public void setValorUnid(String[] valorUnid) {
        this.valorUnid = valorUnid;
    }

    public int[] getUnidade() {
        return unidade;
    }

    public void setUnidade(int[] unidade) {
        this.unidade = unidade;
    }

    public String[] getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(String[] valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public BigDecimal[] getValorTotalV() {
        return valorTotalV;
    }

    public void setValorTotalV(BigDecimal[] valorTotalV) {
        this.valorTotalV = valorTotalV;
    }

    @Override
    public String toString() {
        return "ProdutoModelForm{" +
                "nome=" + Arrays.toString(nome) +
                ", valorUnid=" + Arrays.toString(valorUnid) +
                ", unidade=" + Arrays.toString(unidade) +
                ", valorDesconto=" + Arrays.toString(valorDesconto) +
                ", valorTotal=" + Arrays.toString(valorTotalV) +
                '}';
    }
}
