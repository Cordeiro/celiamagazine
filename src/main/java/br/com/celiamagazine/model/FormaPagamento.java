package br.com.celiamagazine.model;

public enum FormaPagamento {
	DINHEIRO("Dinheiro"),
	DEBITO("Debito"),
	CREDITO("Cartão de Credito");

	private final String nome;

	FormaPagamento(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
