package br.com.celiamagazine.model;

import br.com.celiamagazine.migrate.Convert;
import br.com.celiamagazine.migrate.DBFReader;
import br.com.celiamagazine.migrate.JDBFException;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Venda extends AbstractEntity {

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEmissao;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataVencimento;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRecebimento;
	@NumberFormat(pattern = "###,##0.00")
	private BigDecimal valorFrete;
	//	@NotNull
	@NumberFormat(pattern = "###,##0.00")
	private BigDecimal valorTotal;
	@NumberFormat(pattern = "###,##0.00")
	private BigDecimal valorRecebido;
	private String historico;
	private FormaPagamento formaPagamento;
	private int parcelamento;
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	@ManyToOne
	private Funcionario funcionario;
	@OneToMany
	@JoinColumn(name = "venda_id")
	private List<Venda_Produto> vendaProdutos;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "venda_id")
	private List<Venda_Restante> vendaRestante;
	/*@OneToOne
	private Endereco endereco;*/

	
	/*REMOVER*/
	@Transient
	DBFReader dbfReader;
	
	public Venda(){
		this.cliente = new Cliente();
	}

	public Set<Venda> lerDBF(String path) throws Exception{
		Set<Venda> list = new HashSet<Venda>();
		try {
			dbfReader = new DBFReader(path);
//			for (int i = 0; i < dbfReader.getFieldCount(); i++)
//				System.out.print(dbfReader.getField(i).getName() + "-"); // NOME DOS ATRIBUTOS 
			
			for (; dbfReader.hasNextRecord();) 
				list.add(getDBF(dbfReader));
			
		}catch (JDBFException e) {
			e.printStackTrace();
			System.out.println("Erro! Arquivo '" + path + "'");
		}
		
		return list;
	}
	
	private Venda getDBF(DBFReader dbf) throws Exception{
		Object aobj[];
		Venda venda = new Venda();
		Convert convert = new Convert();
		try {
			aobj = dbf.nextRecord(Charset.forName("ISO8859-1"));
			for (int i = 0;	i< aobj.length; i++) {
				switch (i) {
				case 0: // HISTORICO
					venda.setHistorico(valide(aobj[i].toString()));
					break;
				case 1: // PORTADOR
					break;
				case 2: // DOCUMENTO
					break;
				case 3: // NOME
					venda.getCliente().setNome(aobj[i].toString());
					break;
				case 4:	// EMISSAO
					venda.setDataEmissao(convert.formatData(aobj[i].toString()));
					break;
				case 5:	// VENCIMENTO
					venda.setDataVencimento(convert.formatData(aobj[i].toString()));
					break;
				case 6:	// VALOR_DUPL
					venda.setValorTotal(new BigDecimal(aobj[i].toString()));
					break;
				case 7:	// RECEBIMENT
					venda.setDataRecebimento(convert.formatData(aobj[i].toString()));
					break;
				case 8:	// VALOR_RECE
					venda.setValorRecebido(new BigDecimal(aobj[i].toString()));
					break;
				case 9:	// VALOR_JURO
					break;
				case 10:// ATIVO
					break;
				case 11:// CONTA
					break;
				case 12:// NOSSONUM
					break;
				default:
					throw new Exception("ERRO! Limite ultrapassado");
				}
			}
		} catch (NullPointerException e) {
		} catch (JDBFException e) {
			e.printStackTrace();
		}
		
		return venda;
	}
	
	private String valide(String aux) {
		if (aux.equals("") ) {
			return null;
		}
		return aux;
	}
	
	/*REMOVER*/

	public BigDecimal formatValorString(String valor){
		valor = valor.replace(".","");
		valor = valor.replace(",",".");
		return new BigDecimal(valor);
	}

	public String listProdutos(){
		String linha = "";
		for(Venda_Produto produto : this.vendaProdutos){
			linha = produto.getProduto().getNome() + " / " +linha;
		}
		return linha;
	}

	public List<Venda_Restante> getVendaRestante() {
		return vendaRestante;
	}

	public void setVendaRestante(List<Venda_Restante> vendaRestante) {
		this.vendaRestante = vendaRestante;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(BigDecimal valorFrete) {
		this.valorFrete = valorFrete;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public BigDecimal getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(BigDecimal valorRecebido) {
		this.valorRecebido = valorRecebido;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public List<Venda_Produto> getVendaProdutos() {
		return vendaProdutos;
	}

	public void setVendaProdutos(List<Venda_Produto> vendaProdutos) {
		this.vendaProdutos = vendaProdutos;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(Date dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public int getParcelamento() {
		return parcelamento;
	}

	public void setParcelamento(int parcelamento) {
		this.parcelamento = parcelamento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}


	@Override
	public String toString() {
		return "Venda{" +
				"dataEmissao=" + dataEmissao +
				", dataVencimento=" + dataVencimento +
				", dataRecebimento=" + dataRecebimento +
				", valorTotal=" + valorTotal +
				", valorRecebido=" + valorRecebido +
				", parcelamento=" + parcelamento +
				", cliente=" + cliente.getNome() +
				", id=" + id +
				'}';
	}
}