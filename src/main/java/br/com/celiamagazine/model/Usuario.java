package br.com.celiamagazine.model;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Usuario extends AbstractEntity {

	@NotBlank
	private String nome;
	//@NotBlank
	private String cpf;
	private String rg;
	private String celular;
	private String telefone;
	private String email;
	private String observacao;
	private String imagem;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCadastro;
	//TODO FALTA ADD
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtNascimento;
	//TODO FALTA ADD (FUNC OK)
	private String pai;
	//TODO FALTA ADD (FUNC OK)
	private String mae;
	private String contato;
	private String username;
	private String password;
//  TODO ENDERECO DEVE SER LIST
//	@OneToMany()
//	@JoinColumn(name = "usuario_id")
	@OneToOne
	@NotNull
	@Valid
	private Endereco endereco;
	@OneToOne
	private Role roles;
	@OneToMany
	@JoinColumn(name = "usuario_id")
	private List<Cartao> cartoes;
	@OneToMany(mappedBy = "cliente")
	private List<Venda> vendas;

	public Usuario() {
	}

	public Usuario(String nome, String cpf, String rg, String celular, String telefone, String email, String observacao,
			String imagem, Date dtCadastro, Date dtNascimento, String pai, String mae, String contato, String username,
			String password, Endereco endereco) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.celular = celular;
		this.telefone = telefone;
		this.email = email;
		this.observacao = observacao;
		this.imagem = imagem;
		this.dtCadastro = dtCadastro;
		this.dtNascimento = dtNascimento;
		this.pai = pai;
		this.mae = mae;
		this.contato = contato;
		this.username = username;
		this.password = password;
		this.endereco = endereco;
	}


	public Usuario(String nome, String cpf, String rg, String telefone, String email, String observacao,
			Date dtCadastro, Date dtNascimento, String pai, String mae, String contato) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.telefone = telefone;
		this.email = email;
		this.observacao = observacao;
		this.dtCadastro = dtCadastro;
		this.dtNascimento = dtNascimento;
		this.pai = pai;
		this.mae = mae;
		this.contato = contato;
	}

	public List<Venda> getVendas() {
		return vendas;
	}

	public void setVendas(List<Venda> vendas) {
		this.vendas = vendas;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Cartao> getCartoes() {
		return cartoes;
	}

	public void setCartoes(List<Cartao> cartoes) {
		this.cartoes = cartoes;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Role getRoles() {
		return roles;
	}

	public void setRoles(Role roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Usuario{" +
				"nome='" + nome + '\'' +
				", cpf='" + cpf + '\'' +
				", rg='" + rg + '\'' +
				", celular='" + celular + '\'' +
				", telefone='" + telefone + '\'' +
				", email='" + email + '\'' +
				", dtCadastro=" + dtCadastro +
				", dtNascimento=" + dtNascimento +
				", id=" + id +
				'}';
	}
}