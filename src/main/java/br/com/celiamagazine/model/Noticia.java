package br.com.celiamagazine.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Noticia extends AbstractEntity{

	@NotEmpty
	private String titulo;
	@NotEmpty
	@Lob
	private String noticia;
	@NotEmpty
	@Lob
	private String descricao;
	@Temporal(TemporalType.TIMESTAMP)
	@NotEmpty
	private Date dataPostagem;
	@NotEmpty
	private String tipo;
	private String imagem;
	private String tag;
	@OneToOne
	private Funcionario funcionario;
	
	
	public Funcionario getUsuario() {
		return funcionario;
	}
	public void setUsuario(Funcionario usuario) {
		this.funcionario = usuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getNoticia() {
		return noticia;
	}
	public void setNoticia(String noticia) {
		this.noticia = noticia;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getDataPostagem() {
		return dataPostagem;
	}
	public void setDataPostagem(Date dataPostagem) {
		this.dataPostagem = dataPostagem;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}

	
}