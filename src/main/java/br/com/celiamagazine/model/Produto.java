package br.com.celiamagazine.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class Produto extends AbstractEntity{
	
	@NotEmpty
	private String nome;
	@NotEmpty
	private String descricao;
	@NotNull
	private int quantidade;
	@NotNull
	private int quantidadeMinima;
	@NotEmpty
	private String categoria;
	private String codigoBarras;
	@NotNull
	@NumberFormat(pattern = "###,##0.00")
	private BigDecimal valor;
	@NotEmpty
	private String cor;
	@ElementCollection // TODO DEVE SER OBRIGATORIO
	private List<String> imagens;
	@OneToMany(mappedBy = "produto")
	private List<Comentario> comentarios;
	@NotNull
	@ManyToOne
	@JoinColumn(name="fornecedor_id")
	private Fornecedor fornecedor;
	@ManyToOne
	@JoinColumn(name="promocao_id")
	private Promocao promocao;

	public Produto(){}

	public Produto(Long id) {
		this.id = id;
	}


	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public int getQuantidadeMinima() {
		return quantidadeMinima;
	}
	public void setQuantidadeMinima(int quantidadeMinima) {
		this.quantidadeMinima = quantidadeMinima;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public Promocao getPromocao() {
		return promocao;
	}
	public void setPromocao(Promocao promocao) {
		this.promocao = promocao;
	}
	public String getCodigoBarras() {
		return codigoBarras;
	}
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}
	public List<String> getImagens() {
		return imagens;
	}
	public void setImagens(List<String> imagens) {
		this.imagens = imagens;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public List<Comentario> getComentarios() {
		return comentarios;
	}
	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		System.out.println(valor);
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Produto{" +
				"nome='" + nome + '\'' +
				", quantidade=" + quantidade +
				", quantidadeMinima=" + quantidadeMinima +
				", valor=" + valor +
				", fornecedor=" + fornecedor +
				", id=" + id +
				'}';
	}
}