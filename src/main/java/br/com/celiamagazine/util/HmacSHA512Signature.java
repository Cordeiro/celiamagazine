package br.com.celiamagazine.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

//https://stackoverflow.com/questions/39355241/compute-hmac-sha512-with-secret-key-in-java
public class HmacSHA512Signature {
	private static final String HMAC_SHA512 = "HmacSHA512";
	private static final String consumerKey = "8D9FB8A6C52F4E078EA9F065A30E13C0";
	private static final String HMACsecretKey = "1DCFAB1148F74B9E8344F79F4879E427";
	private static final String url = "celiamagazine.hostinger.com.br";
	private static final String Timestamp = "UTF-8";

	public static String calculateHMAC()
			throws NoSuchAlgorithmException, InvalidKeyException, IllegalStateException, UnsupportedEncodingException {
		SecretKeySpec secretKeySpec = new SecretKeySpec(HMACsecretKey.getBytes(), HMAC_SHA512);
		Mac mac = Mac.getInstance(HMAC_SHA512);
		mac.init(secretKeySpec);
		String base64_code = Base64.getEncoder().encodeToString(mac.doFinal(new String(url + Timestamp).getBytes(Timestamp)));
		return base64_code;
	}

	public static void main(String[] args) throws Exception {
		String hmac = calculateHMAC();
		System.out.println(hmac);
	}
}