package br.com.celiamagazine.util;

import java.math.BigDecimal;
import java.util.Date;

public class Comparator {

    public String valideDate(Date data){
        Date dataAtual = new Date();
        try {
            if (data.before(dataAtual)){
                if(dataAtual.getDay() == data.getDay() && dataAtual.getMonth() == data.getMonth() && dataAtual.getYear() == data.getYear()){
                    return "vencimento";
                }
                return "vencido";
            }
            return "aberto";
        }catch (NullPointerException e){
            e.printStackTrace();
            return "aberto";
        }
    }

    public boolean validePag(BigDecimal valor){
        if(valor == null){
            return false;
        }
        return true;
    }
}
