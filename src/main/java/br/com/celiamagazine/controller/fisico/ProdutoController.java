package br.com.celiamagazine.controller.fisico;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.celiamagazine.dao.FornecedorService;
import br.com.celiamagazine.dao.FuncionarioService;
import br.com.celiamagazine.dao.ProdutoService;
import br.com.celiamagazine.model.Produto;

@Controller
@RequestMapping("produto")	
public class ProdutoController {	//OK
	
	@Autowired FuncionarioService funcionarioService;
	@Autowired FornecedorService fornecedorService;
	@Autowired ProdutoService produtoService;
	
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public ModelAndView produto(Produto produto) {
		ModelAndView view = new ModelAndView("lojaFisica/produto/index","produto", produto);
		view.addObject("fornecedores",fornecedorService.findAll());
		view.addObject("breadcrumb", "Produto / Novo");
		return view;
	}
	
	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView novo(@Valid Produto produto, BindingResult bindingFuncionario, RedirectAttributes redirectAttributes) {
		if(bindingFuncionario.hasErrors())
			return produto(produto);
		produtoService.save(produto);
		redirectAttributes.addFlashAttribute("msgSuccess", "Produto salvo com sucesso!");
		return new ModelAndView("redirect:listar");
	}
	
	@RequestMapping(value = "/buscar/nome", method = RequestMethod.GET)
	public ModelAndView buscarNome(@RequestParam String nome) {
		return new ModelAndView("lojaFisica/produto/listar","produtos",produtoService.findByNomeContainingIgnoreCase(nome));
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public ModelAndView buscarId(@RequestParam Long id) {
		ModelAndView view = new ModelAndView("lojaFisica/produto/index","produto", produtoService.findOne(id));
		view.addObject("breadcrumb", "Produto / Buscar");
		view.addObject("fornecedores",fornecedorService.findAll());
		return view;
	}

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public ModelAndView listar() {
		return new ModelAndView("lojaFisica/produto/listar","produtos",produtoService.findAll());
	}

	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
	public ModelAndView remover(Long id, RedirectAttributes attributes) {
		produtoService.delete(id);
		attributes.addFlashAttribute("msgSuccess","Produto removido com sucesso!");
		return new ModelAndView("redirect:listar"); 
	}
}