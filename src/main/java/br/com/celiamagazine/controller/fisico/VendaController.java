package br.com.celiamagazine.controller.fisico;

import br.com.celiamagazine.dao.*;
import br.com.celiamagazine.model.*;
import br.com.celiamagazine.util.Comparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("venda")
public class VendaController {

    @Autowired VendaService vendaService;
    @Autowired VendaProdutoService vendaProdutoService;
    @Autowired VendaRestanteService vendaRestoService;
    @Autowired FuncionarioService funcionarioService;
    @Autowired EnderecoService enderecoService;
    @Autowired ClienteService clienteService;
    @Autowired ProdutoService produtoService;

    @RequestMapping(value = "/novo", method = RequestMethod.GET) // OK
    public ModelAndView index(Venda venda) {
        ModelAndView view = new ModelAndView("lojaFisica/venda/novo","venda", venda);
        view.addObject("clientes",clienteService.findAll());
        view.addObject("produtos",produtoService.findAll());
        view.addObject("funcionarios",funcionarioService.findAll());
        view.addObject("breadcrumb", "Venda / Novo");
        /*view.addObject("estados", Estado.values());*/
        view.addObject("formaPagamento", FormaPagamento.values());
        return view;
    }

    @RequestMapping(value = "/novo", method = RequestMethod.POST) // OK
    public ModelAndView novo(@Valid Venda venda, BindingResult binding, RedirectAttributes redirectAttributes, ProdutoModelForm produtos) {
        if(binding.hasErrors())
            return index(venda);
        venda.getCliente().setId(clienteService.findByNome(venda.getCliente().getNome()));
        if(!venda.getFuncionario().getNome().equals(""))
            venda.getFuncionario().setId(funcionarioService.findByNome(venda.getFuncionario().getNome()));
        else venda.setFuncionario(null);
        venda.setDataEmissao(new Date());
        venda.setDataVencimento(new Date());
        venda.getDataVencimento().setMonth(venda.getDataVencimento().getMonth() + 1); // TODO SE DEZEMBRO GERA ERRO, TRATAR ISSO
        venda.setVendaProdutos(new ArrayList<>());
        venda.setValorTotal(new BigDecimal("0"));

        List<Venda_Produto> produtoList = new ArrayList<>();
        for (int i = 0; i < produtos.getNome().length; i++){
           Venda_Produto vendaProduto = new Venda_Produto( new Produto(produtoService.findByNome(produtos.getNome()[i]))
                    , produtos.getValorDesconto()[i], produtos.getUnidade()[i]);
            venda.getVendaProdutos().add(vendaProdutoService.save(vendaProduto));
            System.out.println("Valor total : "+venda.getValorTotal()+" Soma : "+venda.getValorTotal().add(produtos.getValorTotalV()[i]));
            venda.setValorTotal(venda.getValorTotal().add(produtos.getValorTotalV()[i]));
        }
        vendaService.save(venda);
        redirectAttributes.addFlashAttribute("msgSuccess", "Venda salva com sucesso!");
        return new ModelAndView("redirect:listar");
    }

    @RequestMapping(value = "/buscar/nome", method = RequestMethod.GET)
    public ModelAndView buscarNome(@RequestParam String nome) {
        List<Venda> vendas = vendaService.findAllVendasClienteNome("%"+nome+"%");
        try {
            for (Venda venda : vendas){
                venda.getCliente().setVendas(null);
                venda.getFuncionario().setVendas(null);
                for(Venda_Produto v : venda.getVendaProdutos()){
                    v.getProduto().setFornecedor(null);
                }
            }
        }catch (NullPointerException e){ }
        ModelAndView view = new ModelAndView("lojaFisica/venda/listar","vendas",vendas);
        view.addObject("breadcrumb", "Venda / Buscar");
        view.addObject("formaPagamento", FormaPagamento.values());
        view.addObject("comparator", new Comparator());
        return view;
    }

    /*@RequestMapping(value = "/buscar", method = RequestMethod.GET)
    public ModelAndView buscarId(@RequestParam Long id) { // TODO ALTERAR PRA VENDA
        ModelAndView view = new ModelAndView("lojaFisica/cliente/index","cliente", clienteService.findOne(id));
        view.addObject("estados", Estado.values());
        view.addObject("breadcrumb", "Cliente / Buscar");
        view.addObject("fornecedores",clienteService.findAll());
        return view;
    }*/

    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public ModelAndView listar() {
        ModelAndView view = new ModelAndView("lojaFisica/venda/listar","vendas",vendaService.findAllOrderByCliente(new PageRequest(0,200)));
        view.addObject("breadcrumb", "Venda / Listar");
        view.addObject("formaPagamento", FormaPagamento.values());
        view.addObject("comparator", new Comparator());
        return view;
    }

    @RequestMapping(value = "/remover", method = RequestMethod.POST)
    @ResponseBody
    public String remover(Long vendaId, Long restoId) {
        System.out.println("\nVenda: "+vendaId+ "\tResto: "+restoId);
        Venda venda = vendaService.findOne(vendaId);
        if(restoId != null){
            Venda_Restante restante = null;
            for(Venda_Restante resto : venda.getVendaRestante()){
                if(resto.getId().equals(restoId)){
                    restante = resto;
                    break;
                }
            }
            restante.setVenda(null);
            venda.getVendaRestante().remove(restante);
            vendaRestoService.delete(restoId);
            vendaService.save(venda);
        }else{
            for (Venda_Produto vendaProduto : venda.getVendaProdutos()){
                vendaProdutoService.delete(vendaProduto.getId());
            }
            for(Venda_Restante resto : venda.getVendaRestante()){
                vendaRestoService.delete(resto.getId());
            }
            vendaService.delete(vendaId);
        }

        return "Venda removido com sucesso!";
    }

    @RequestMapping(value = "/pagamento", method = RequestMethod.POST) // ALTERAR POR AJAX
    public ModelAndView pagamento(Long vendaId, Long restoId, String valor, RedirectAttributes attributes) {
        Venda venda = vendaService.findOne(vendaId);
        BigDecimal valorPago = venda.formatValorString(valor);
        BigDecimal restante = new BigDecimal("0");
        System.out.println("\nVenda: "+vendaId+ "\tResto: "+restoId);

        if(restoId != null){
            for (Venda_Restante resto : venda.getVendaRestante()){
                if(resto.getId().equals(restoId)){
                    resto.setDataRecebimento(new Date());
                    resto.setValorRecebido(valorPago);
                    restante = resto.getValorTotal().subtract(resto.getValorRecebido());
                }
            }
        }else{
            venda.setDataRecebimento(new Date());
            venda.setValorRecebido(valorPago);
            restante = venda.getValorTotal().subtract(venda.getValorRecebido());
        }

        if(restante.signum() == 1){
            Venda_Restante vendaRestante = new Venda_Restante(venda, restante);
            venda.getVendaRestante().add(vendaRestante);
        }
        vendaService.save(venda);
        attributes.addFlashAttribute("msgSuccess","Pagamento realizado com sucesso!");
        return new ModelAndView("redirect:listar");
    }

    @RequestMapping(value = "/buscarVendas", method = RequestMethod.POST)
    @ResponseBody
    public Venda buscaVenda(Long vendaId) {
        System.out.println("ID: "+vendaId);
        Venda venda = vendaService.findOne(vendaId);
        if(venda.getFuncionario() != null)
            venda.getFuncionario().setVendas(null);
        else{
            venda.setFuncionario(new Funcionario());
            venda.getFuncionario().setNome("");
        }

        for(Venda_Produto vendas : venda.getVendaProdutos()){
            vendas.getProduto().setFornecedor(null);
        }
        for(Venda_Restante resto : venda.getVendaRestante()){
            resto.setVenda(null);
        }
        venda.getCliente().setVendas(null);
        return venda;
    }

}