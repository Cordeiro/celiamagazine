package br.com.celiamagazine.controller.fisico;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.celiamagazine.controller.AbstractController;

@Controller
@RequestMapping("dashboard")
public class BashboardController extends AbstractController{

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index() {
		return new ModelAndView("lojaFisica/indexDashboard");
	}
		
	
//	@RequestMapping(value = "/{codigo}", method = RequestMethod.POST)
//	@RequestMapping(value = "/{codigo}", method = RequestMethod.POST)
}
