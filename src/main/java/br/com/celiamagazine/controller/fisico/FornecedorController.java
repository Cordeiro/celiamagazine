package br.com.celiamagazine.controller.fisico;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.celiamagazine.dao.EnderecoService;
import br.com.celiamagazine.dao.FornecedorService;
import br.com.celiamagazine.model.Estado;
import br.com.celiamagazine.model.Fornecedor;

@Controller
@RequestMapping("fornecedor")
public class FornecedorController {
	
	@Autowired FornecedorService fornecedorService;
	@Autowired EnderecoService enderecoService;
	
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public ModelAndView index(Fornecedor fornecedor) {
		ModelAndView view = new ModelAndView("lojaFisica/fornecedor/index","fornecedor", fornecedor);
		view.addObject("breadcrumb", "Fornecedor / Novo");
		view.addObject("estados", Estado.values());
		return view;
	}
	
	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView novo(@Valid Fornecedor fornecedor, BindingResult binding, RedirectAttributes redirectAttributes) {
		if(binding.hasErrors())
			return index(fornecedor);
		enderecoService.save(fornecedor.getEndereco());
		fornecedorService.save(fornecedor);
		redirectAttributes.addFlashAttribute("msgSuccess", "Fornecedor salvo com sucesso!");
		return new ModelAndView("redirect:listar");
	}
	
	@RequestMapping(value = "/buscar/nome", method = RequestMethod.GET)
	public ModelAndView buscarNome(@RequestParam String nome) {
		return new ModelAndView("lojaFisica/fornecedor/listar","fornecedores",fornecedorService.findByRazaoSocialContainingIgnoreCase(nome));
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public ModelAndView buscarId(@RequestParam Long id) {
		ModelAndView view = new ModelAndView("lojaFisica/fornecedor/index","fornecedor", fornecedorService.findOne(id));
		view.addObject("estados", Estado.values());
		view.addObject("breadcrumb", "Fornecedor / Buscar");
		view.addObject("fornecedores",fornecedorService.findAll());
		return view;
	}

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public ModelAndView listar() {
		return new ModelAndView("lojaFisica/fornecedor/listar","fornecedores",fornecedorService.findAll());
	}

	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
	public ModelAndView remover(Long idForn, Long idEnd, RedirectAttributes attributes) {
		fornecedorService.delete(idForn);
		enderecoService.delete(idEnd);
		attributes.addFlashAttribute("msgSuccess","Fornecedor removido com sucesso!");
		return new ModelAndView("redirect:listar"); 
	}

}
