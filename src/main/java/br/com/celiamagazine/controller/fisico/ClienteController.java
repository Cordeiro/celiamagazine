package br.com.celiamagazine.controller.fisico;

import br.com.celiamagazine.dao.ClienteService;
import br.com.celiamagazine.dao.EnderecoService;
import br.com.celiamagazine.model.Cliente;
import br.com.celiamagazine.model.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("cliente")
public class ClienteController {
	
	@Autowired ClienteService clienteService;
	@Autowired EnderecoService enderecoService;
	
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public ModelAndView index(Cliente cliente) {
		ModelAndView view = new ModelAndView("lojaFisica/cliente/index","cliente", cliente);
		view.addObject("breadcrumb", "Cliente / Novo");
		view.addObject("estados", Estado.values());
		return view;
	}
	
	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView novo(@Valid Cliente cliente, BindingResult binding, RedirectAttributes redirectAttributes) {
		if(binding.hasErrors())
			return index(cliente);
		enderecoService.save(cliente.getEndereco());
		clienteService.save(cliente);
		redirectAttributes.addFlashAttribute("msgSuccess", "Cliente salvo com sucesso!");
		return new ModelAndView("redirect:listar");
	}
	
	@RequestMapping(value = "/buscar/nome", method = RequestMethod.GET)
	public ModelAndView buscarNome(@RequestParam String nome) {
		return new ModelAndView("lojaFisica/cliente/listar","clientes",clienteService.findByNomeContainingIgnoreCase(nome));
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public ModelAndView buscarId(@RequestParam Long id) {
		ModelAndView view = new ModelAndView("lojaFisica/cliente/index","cliente", clienteService.findOne(id));
		view.addObject("estados", Estado.values());
		view.addObject("breadcrumb", "Cliente / Buscar");
		view.addObject("fornecedores",clienteService.findAll());
		return view;
	}

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public ModelAndView listar() {
		return new ModelAndView("lojaFisica/cliente/listar","clientes",clienteService.findAllByOrderByNomeAsc(new PageRequest(0,200)));
	}

	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
	public ModelAndView remover(Long idCli, Long idEnd, RedirectAttributes attributes) {
		clienteService.delete(idCli);
		enderecoService.delete(idEnd);
		attributes.addFlashAttribute("msgSuccess","Cliente removido com sucesso!");
		return new ModelAndView("redirect:listar"); 
	}
}
