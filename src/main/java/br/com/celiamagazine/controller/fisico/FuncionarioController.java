package br.com.celiamagazine.controller.fisico;

import br.com.celiamagazine.dao.EnderecoService;
import br.com.celiamagazine.dao.FuncionarioService;
import br.com.celiamagazine.model.Cargo;
import br.com.celiamagazine.model.Estado;
import br.com.celiamagazine.model.Funcionario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("funcionario")	//OK
public class FuncionarioController {
	
	@Autowired FuncionarioService funcionarioService;
	@Autowired EnderecoService enderecoService;
	
	@RequestMapping(value = "/perfil", method = RequestMethod.GET)
	public ModelAndView perfil(Funcionario funcionario) { // TODO PERTENCE AO USUARIO DA SESSAO
		List<Funcionario> list = funcionarioService.findAll();
		Funcionario newFuncionario;
		try {
			newFuncionario = list.get(0);
		}catch (IndexOutOfBoundsException e){
			newFuncionario = new Funcionario();
		}
		ModelAndView view = new ModelAndView("lojaFisica/funcionario/perfil","funcionario",newFuncionario);
		view.addObject("cargos", Cargo.values());
		view.addObject("estados", Estado.values());
		return view;
	}
	
	@RequestMapping(value = "/buscar/nome", method = RequestMethod.GET)
	public ModelAndView buscarNome(@RequestParam String nome) {
		return new ModelAndView("lojaFisica/funcionario/listar","funcionarios",funcionarioService.findByNomeContainingIgnoreCase(nome));
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public ModelAndView buscarId(@RequestParam Long id) {
		ModelAndView view = new ModelAndView("lojaFisica/funcionario/index","funcionario", funcionarioService.findOne(id));
		view.addObject("breadcrumb", "Funcionário / Buscar");
		view.addObject("cargos", Cargo.values());
		view.addObject("estados", Estado.values());
		return view;
	}

	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public ModelAndView funcionario(Funcionario funcionario) {
		ModelAndView view = new ModelAndView("lojaFisica/funcionario/index","funcionario", funcionario);
		view.addObject("breadcrumb", "Funcionário / Novo");
		view.addObject("cargos", Cargo.values());
		view.addObject("estados", Estado.values());
		return view;
	}

	@RequestMapping(value = "/novo", method = RequestMethod.POST)
	public ModelAndView novo(@Valid Funcionario funcionario, BindingResult bindingFuncionario, RedirectAttributes redirectAttributes) {
		if(bindingFuncionario.hasErrors())
			return funcionario(funcionario);
		enderecoService.save(funcionario.getEndereco());
		funcionarioService.save(funcionario);
		redirectAttributes.addFlashAttribute("msgSuccess", "Funcionário salvo com sucesso");
		return new ModelAndView("redirect:listar");
	}

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public ModelAndView listar() {
		return new ModelAndView("lojaFisica/funcionario/listar","funcionarios",funcionarioService.findAll());
	}

	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
	public ModelAndView remover(Long idFunc, Long idEnd, RedirectAttributes redirectAttributes) {
		funcionarioService.delete(idFunc);
		enderecoService.delete(idEnd);
		redirectAttributes.addFlashAttribute("msgSuccess", "Funcionário removido com sucesso");
		return new ModelAndView("redirect:listar"); 
	}
}
