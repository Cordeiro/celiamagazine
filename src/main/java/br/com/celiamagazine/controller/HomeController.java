package br.com.celiamagazine.controller;

import br.com.celiamagazine.dao.ClienteService;
import br.com.celiamagazine.dao.EnderecoService;
import br.com.celiamagazine.dao.FornecedorService;
import br.com.celiamagazine.dao.VendaService;
import br.com.celiamagazine.migrate.Imports;
import br.com.celiamagazine.model.Cliente;
import br.com.celiamagazine.model.Fornecedor;
import br.com.celiamagazine.model.Venda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("tests")
public class HomeController {
	
	HttpSession session;
	HttpServletResponse response;
	@Autowired ClienteService clienteService;
	@Autowired EnderecoService enderecoService;
	@Autowired FornecedorService fornecedorService;
	@Autowired VendaService vendaService;
	
	
	@GetMapping("/")
	public String listar() {
		ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        System.out.println("DATETIME = " + Date.from(utc.toInstant()));
        System.out.println(ZoneOffset.UTC);
        System.out.println("Sessao: " + session.getId());
        
		return "home";
	}
	
	@GetMapping("sendAjax")
	public void ajax() {
		
	}
	
	
	@GetMapping("migrate")
	public String migrate() {
		Set<Cliente> setClientes = new HashSet<>();
		Set<Venda> setVendas = new HashSet<>();
		Set<Fornecedor> setFornecedores = new HashSet<>();
		
		System.out.println("Iniciando conversão...");
		
		for (Imports arquivo : Imports.values()) {
			System.out.println("\n\nLendo " +arquivo);
			
			try {
				switch (arquivo) {
				case CLIENTE:
					setClientes.addAll(new Cliente().lerDBF(arquivo.getUrl()));
					break;
				case VENDA:
					setVendas.addAll(new Venda().lerDBF(arquivo.getUrl()));
					break;
				case FORNECEDOR:
					setFornecedores.addAll(new Fornecedor().lerDBF(arquivo.getUrl()));
					break;
				default:
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("Salvando Enderecos e Clientes...");
		for (Cliente cliente : setClientes) {
			enderecoService.save(cliente.getEndereco());
			clienteService.save(cliente);
		}
		System.out.println("Salvando Vendas...");

		int count = 0;
		Set<Venda> newVendas = ligarVendaCliente(setClientes,setVendas);
		for(Venda venda : newVendas){
			if (venda.getCliente().getId() == null){
				count++;
				System.out.println(venda.getCliente());
			}
		}
		System.out.println("Id nulo: "+count);

		vendaService.save(newVendas);

		System.out.println("Salvando Fornecedores...");
		for(Fornecedor fornecedor: setFornecedores){
			enderecoService.save(fornecedor.getEndereco());
			fornecedorService.save(fornecedor);
		}

//				Set<Cliente> clientesSave = new HashSet<>(clienteService.saveCollections(setClientes));
//				Set<Venda> newVendas = ligarVendaCliente(clientesSave, setVendas);
//				vendaService.saveCollections(newVendas);
//				for (Venda venda : newVendas) {
//					System.out.println(venda);
//				}
		System.out.println("CONCLUIDO!");
		return "home";
	}

	private Set<Venda> ligarVendaCliente(Set<Cliente> clientes, Set<Venda> vendas) {
		for (Venda venda : vendas) {
			for (Cliente cliente : clientes) {
				if (venda.getCliente().getNome().equals(cliente.getNome())) {
					venda.setCliente(cliente);
				}
			}
		}
		Set<Venda> newVendas = new HashSet<>();
		for (Venda venda : vendas) {
			if(venda.getCliente().getId() != null){
				newVendas.add(venda);
			}
		}
		return newVendas;
	}
	
}