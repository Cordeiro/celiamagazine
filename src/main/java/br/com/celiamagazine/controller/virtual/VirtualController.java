package br.com.celiamagazine.controller.virtual;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class VirtualController {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index() {
		
		return new ModelAndView("index");
	}

	/*@RequestMapping(value = "/header", method = RequestMethod.GET)
	public ModelAndView header() {

		return new ModelAndView("lojaVirtual/header");
	}*/
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home() {
		
		return new ModelAndView("lojaVirtual/home");
	}
	
	@RequestMapping(value = "/carrinho", method = RequestMethod.GET)
	public String carrinho() {
		return "lojaVirtual/carrinho";
	}

	@RequestMapping(value = "/produtos", method = RequestMethod.GET)
	public String listar() {
		return "lojaVirtual/lista";
	}
	
	//@RequestMapping(value = "/produto/{slug}", method = RequestMethod.GET) TROCAR POR ESTE
	@RequestMapping(value = "/produto", method = RequestMethod.GET)
	public String produto(String slug) {
		return "lojaVirtual/produto";
	}
	
	@RequestMapping(value = "/sobre", method = RequestMethod.GET)
	public String sobre() {
		return "lojaVirtual/about";
	}
}