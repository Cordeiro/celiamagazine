package br.com.celiamagazine.migrate;

import java.text.*;
import java.util.Date;

public class JDBField {

	private String name;
	private char type;
	private int length;
	private int decimalCount;

	public JDBField(String s, char c, int i, int j) throws JDBFException {
		if (s.length() > 10) {
			throw new JDBFException("O nome do campo tem mais de 10 caracteres: " + s);
		}
		if (c != 'C' && c != 'N' && c != 'L' && c != 'D' && c != 'F') {
			throw new JDBFException("O tipo de campo não é válido. Obteve: " + c);
		}
		if (i < 1) {
			throw new JDBFException("O comprimento do campo deve ser um número inteiro positivo. Obteve: " + i);
		}
		if (c == 'C' && i >= 255) {
			throw new JDBFException(
					"O comprimento do campo deve ser inferior a 255 caracteres para campos de caracteres. Obteve: " + i);
		}
		if (c == 'N' && i >= 21) {
			throw new JDBFException("O comprimento do campo deve ser inferior a 21 dígitos para campos numéricos. Obteve: " + i);
		}
		if (c == 'L' && i != 1) {
			throw new JDBFException("O comprimento do campo deve ser de 1 caractere para campos lógicos. Obteve: " + i);
		}
		if (c == 'D' && i != 8) {
			throw new JDBFException("O comprimento do campo deve ser de 8 caracteres para os campos da data. Obteve: " + i);
		}
		if (c == 'F' && i >= 21) {
			throw new JDBFException(
					"O comprimento do campo deve ser inferior a 21 dígitos para campos de ponto flutuante. Obteve: " + i);
		}
		if (j < 0) {
			throw new JDBFException("A contagem decimal de campo não deve ser um número inteiro negativo. Obteve: " + j);
		}
		if ((c == 'C' || c == 'L' || c == 'D') && j != 0) {
			throw new JDBFException(
					"A contagem decimal de campo deve ser 0 para caracteres, lógicos e campos de data. Obteve: " + j);
		}
		if (j > i - 1) {
			throw new JDBFException("A contagem decimal de campo deve ser inferior ao comprimento - 1. Obteve: " + j);
		} else {
			name = s;
			type = c;
			length = i;
			decimalCount = j;
			return;
		}
	}

	public String format(Object obj) throws JDBFException {
		if (type == 'N' || type == 'F') {
			if (obj == null) {
				obj = new Double(0.0D);
			}
			if (obj instanceof Number) {
				Number number = (Number) obj;
				StringBuffer stringbuffer = new StringBuffer(getLength());
				for (int i = 0; i < getLength(); i++) {
					stringbuffer.append("#");
				}
				if (getDecimalCount() > 0) {
					stringbuffer.setCharAt(getLength() - getDecimalCount() - 1, '.');
				}
				DecimalFormat decimalformat = new DecimalFormat(stringbuffer.toString());
				String s1 = decimalformat.format(number);
				int k = getLength() - s1.length();
				if (k < 0) {
					throw new JDBFException("Valor " + number + " não pode caber no padrão: '" + stringbuffer + "'.");
				}
				StringBuffer stringbuffer2 = new StringBuffer(k);
				for (int l = 0; l < k; l++) {
					stringbuffer2.append(" ");
				}
				return stringbuffer2 + s1;
			} else {
				throw new JDBFException("Esperava um número, obteve " + obj.getClass() + ".");
			}
		}
		if (type == 'C') {
			if (obj == null) {
				obj = "";
			}
			if (obj instanceof String) {
				String s = (String) obj;
				if (s.length() > getLength()) {
					throw new JDBFException("'" + obj + "' é mais longo do que " + getLength() + " caracteres.");
				}
				StringBuffer stringbuffer1 = new StringBuffer(getLength() - s.length());
				for (int j = 0; j < getLength() - s.length(); j++) {
					stringbuffer1.append(' ');
				}
				return s + stringbuffer1;
			} else {
				throw new JDBFException("Esperava um String, obteve " + obj.getClass() + ".");
			}
		}
		if (type == 'L') {
			if (obj == null) {
				obj = new Boolean(false);
			}
			if (obj instanceof Boolean) {
				Boolean boolean1 = (Boolean) obj;
				return boolean1.booleanValue() ? "Y" : "N";
			} else {
				throw new JDBFException("Esperava um booleano, obteve " + obj.getClass() + ".");
			}
		}
		if (type == 'D') {
			if (obj == null) {
				obj = new Date();
			}
			if (obj instanceof Date) {
				Date date = (Date) obj;
				SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMdd");
				return simpledateformat.format(date);
			} else {
				throw new JDBFException("Esperava uma Data, obteve " + obj.getClass() + ".");
			}
		} else {
			throw new JDBFException("Tipo de JDBFField não reconhecido: " + type);
		}
	}

	public Object parse(String s) throws JDBFException {
		s = s.trim();
		if (type == 'N' || type == 'F') {
			if (s.equals("")) {
				s = "0";
			}
			try {
				if (getDecimalCount() == 0) {
					return new Long(s);
				} else {
					return new Double(s);
				}
			} catch (NumberFormatException numberformatexception) {
				throw new JDBFException(numberformatexception);
			}
		}
		if (type == 'C') {
			return s;
		}
		if (type == 'L') {
			if (s.equals("Y") || s.equals("y") || s.equals("T") || s.equals("t")) {
				return new Boolean(true);
			}
			if (this.name.equals("ATIVO") || s.equals("N") || s.equals("n") || s.equals("F") || s.equals("f")) {
				return new Boolean(false);
			} else {
				throw new JDBFException("Valor não reconhecido para o campo lógico: " + s + " field: "+this.name);
			}
		}
		if (type == 'D') {
			SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMdd");
			try {
				if ("".equals(s)) {
					return null;
				} else {
					return simpledateformat.parse(s);
				}
			} catch (ParseException parseexception) {
				throw new JDBFException(parseexception);
			}
		} else {
			throw new JDBFException("Tipo de JDBFField não reconhecido: " + type);
		}
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getDecimalCount() {
		return decimalCount;
	}

	public void setDecimalCount(int decimalCount) {
		this.decimalCount = decimalCount;
	}

	public String toString() {
		return name;
	}
}