package br.com.celiamagazine.migrate;

public enum Imports {
	CLIENTE("./dbf/CLIENTES.DBF"),
	VENDA("./dbf/RECEBER.DBF"),
	FORNECEDOR("./dbf/FORNECED.DBF");
	
	private String url;

	private Imports(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}