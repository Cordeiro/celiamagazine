package br.com.celiamagazine.migrate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Convert {
	public Date formatData(String data) throws Exception {
		String ano = "";
		String dia = data.substring(8, 10);
		String mes = data.substring(4, 7);
		try {
			//System.out.print("Antes "+dia+"/"+mes+"/"+ano);

			if (data.length() == 34)
				ano = data.substring(data.length()-4, data.length());

			if (dia.length() != 2) {
				System.out.println("Valor do dia errado: " + dia);
				return null;
			}
			if (mes.length() != 3) {
				System.out.println("Valor do mês errado: " + mes);
				return null;
			}
			if (ano.length() != 4 || Integer.parseInt(ano) < 1900 || Integer.parseInt(ano) > Calendar.getInstance().getWeekYear()+1) {
//				System.out.println("Valor de ano errado: " + ano + "\tT: " + ano.length() +
//						"\tComparado com " + (Calendar.getInstance().getWeekYear()+1));
				return null;
			}
			int mesValue = Mes.valueOf(mes).ordinal()+1;
			if(mesValue < 10)
				mes = "0" + mesValue;
			else mes = String.valueOf(mesValue);

			//System.out.println("\tDepois: "+dia+"/"+mes+"/"+ano);
		} catch (StringIndexOutOfBoundsException e) {
			return null;
		}
		
		return new SimpleDateFormat("dd/MM/yyyy").parse(dia+"/"+mes+"/"+ano);
	}
	
}