/**
 * <p>Copyright: Copyright (c) 2004~2012~2012</p>
 * <p>Company: iihero.com</p>
 * @author : He Xiong
 * @version 1.1
 */

package br.com.celiamagazine.migrate;

import java.io.PrintStream;
import java.io.PrintWriter;

public class JDBFException extends Exception {

	private static final long serialVersionUID = 6528307262883474706L;

	private Throwable detail;
	
	public JDBFException(String s) {
		this(s, null);
	}

	public JDBFException(Throwable throwable) {
		this(throwable.getMessage(), throwable);
	}

	public JDBFException(String s, Throwable throwable) {
		super(s);
		detail = throwable;
	}

	public String getMessage() {
		if (detail == null) {
			return super.getMessage();
		} else {
			return super.getMessage();
		}
	}

	public void printStackTrace(PrintStream printstream) {
		if (detail == null) {
			super.printStackTrace(printstream);
			return;
		}
		PrintStream printstream1 = printstream;
		printstream1.println(this);
		detail.printStackTrace(printstream);
		return;
	}

	public void printStackTrace() {
		printStackTrace(System.err);
	}

	public void printStackTrace(PrintWriter printwriter) {
		if (detail == null) {
			super.printStackTrace(printwriter);
			return;
		}
		PrintWriter printwriter1 = printwriter;

		printwriter1.println(this);
		detail.printStackTrace(printwriter);
		return;
	}
}