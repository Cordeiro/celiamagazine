package br.com.celiamagazine.migrate;

public enum Mes {
	Jan, 
	Feb, 
	Mar, 
	Apr, 
	May, 
	Jun, 
	Jul, 
	Aug, 
	Sep,
	Oct,
	Nov,
	Dec;

}