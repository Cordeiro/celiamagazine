package br.com.celiamagazine.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.celiamagazine.model.Usuario;

@Repository
@Transactional
public class UsuarioDAO {
	
	@PersistenceContext
	EntityManager entityManager;
	
	public Usuario getUser(String nome) {
		return entityManager.createQuery("SELECT * FROM usuario u WHERE u=:nome",Usuario.class)
			.setParameter("nome", nome).getSingleResult();
	}
	
	public Usuario save(Usuario usuario) {
		entityManager.persist(usuario);
		return usuario;
	}
}