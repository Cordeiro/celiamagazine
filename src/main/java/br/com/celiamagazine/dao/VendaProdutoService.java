package br.com.celiamagazine.dao;

import br.com.celiamagazine.model.Venda_Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendaProdutoService extends JpaRepository<Venda_Produto, Long>{

}
