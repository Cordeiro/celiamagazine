package br.com.celiamagazine.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiamagazine.model.Fornecedor;

public interface FornecedorService extends JpaRepository<Fornecedor, Long>{

	List<Fornecedor> findByRazaoSocialContainingIgnoreCase(String nome);
	
}