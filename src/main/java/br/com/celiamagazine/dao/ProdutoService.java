package br.com.celiamagazine.dao;

import br.com.celiamagazine.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProdutoService extends JpaRepository<Produto, Long>{

	List<Produto> findByNomeContainingIgnoreCase(String nome);

	@Query("SELECT p.id FROM Produto p WHERE p.nome = ?1")
	Long findByNome(String nome);
}
