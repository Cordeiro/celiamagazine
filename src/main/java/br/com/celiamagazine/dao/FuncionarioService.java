package br.com.celiamagazine.dao;

import br.com.celiamagazine.model.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FuncionarioService extends JpaRepository<Funcionario, Long>{
	
	public List<Funcionario> findByNomeContainingIgnoreCase(String nome);

    @Query("SELECT f.id FROM Funcionario f WHERE f.nome = ?1")
	Long findByNome(String nome);

}