package br.com.celiamagazine.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.celiamagazine.model.AbstractEntity;


@Repository
@Transactional
public class AbstractDAO<T extends AbstractEntity> {
	
	@PersistenceContext
	EntityManager entityManager;
	
	public Collection<T> saveCollections(Collection<T> entity){
		try {
			for (T t : entity) {
				entityManager.persist(t);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return entity;
	}
	
	public void save(T entity) {
		entityManager.persist(entity);
	}
	
	
	
}
