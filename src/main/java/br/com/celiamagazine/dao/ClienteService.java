package br.com.celiamagazine.dao;

import br.com.celiamagazine.model.Cliente;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClienteService extends JpaRepository<Cliente, Long>{

	List<Cliente> findByNomeContainingIgnoreCase(String nome);

	@Query("SELECT c.id FROM Cliente c WHERE c.nome = ?1")
    Long findByNome(String nome);

	List<Cliente> findAllByOrderByNomeAsc(Pageable page);
}
