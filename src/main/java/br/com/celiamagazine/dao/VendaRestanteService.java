package br.com.celiamagazine.dao;

import br.com.celiamagazine.model.Venda_Restante;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendaRestanteService extends JpaRepository<Venda_Restante, Long> {


}
