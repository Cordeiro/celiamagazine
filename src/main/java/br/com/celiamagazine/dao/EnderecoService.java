package br.com.celiamagazine.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiamagazine.model.Endereco;

public interface EnderecoService extends JpaRepository<Endereco, Long>{

}
