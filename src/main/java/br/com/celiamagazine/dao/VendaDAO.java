package br.com.celiamagazine.dao;

import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.celiamagazine.model.Cliente;
import br.com.celiamagazine.model.Fornecedor;
import br.com.celiamagazine.model.Venda;

@Repository
@Transactional
public class VendaDAO extends AbstractDAO<Venda> {
	
	public void saveMigrate(Set<Cliente> setClientes, Set<Venda> setVendas, Set<Fornecedor> setFornecedores) {
		Set<Venda> newVendas = ligarVendaCliente(setClientes, setVendas);
		this.saveCollections(newVendas);
		// Set<Cliente> clientesSave = new
		// HashSet<>(clienteDAO.saveCollections(setClientes));
	}

	private Set<Venda> ligarVendaCliente(Set<Cliente> clientes, Set<Venda> vendas) {
		for (Venda venda : vendas) {
			for (Cliente cliente : clientes) {
				if (venda.getCliente().getNome().equals(cliente.getNome())) {
					venda.setCliente(cliente);
					// cliente.getVendas().add(venda);
					break;
				}
			}
		}
		return vendas;
	}
}
