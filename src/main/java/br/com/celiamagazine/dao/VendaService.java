package br.com.celiamagazine.dao;

import br.com.celiamagazine.model.Venda;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VendaService extends JpaRepository<Venda, Long>{

    @Query(value = "SELECT v FROM Venda v ORDER BY v.cliente.nome ASC, v.dataEmissao")
    public List<Venda> findAllOrderByCliente(Pageable pageable);

    @Query("SELECT v FROM Venda v WHERE UPPER(v.cliente.nome) LIKE UPPER(?1)")
    List<Venda> findAllVendasClienteNome(String nome);

    List<Venda> findAllById(Long id);
}