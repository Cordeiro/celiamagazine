package br.com.migrate;

import br.com.celiamagazine.dao.ClienteService;
import br.com.celiamagazine.model.Cliente;
import br.com.migrate.dao.VendaDAO;
import br.com.migrate.model.ClienteMigrate;
import br.com.migrate.model.Endereco;
import br.com.migrate.model.Fornecedor;
import br.com.migrate.model.Venda;
import br.com.migrate.util.Imports;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class SuiteImportAll {
	
	static Set<ClienteMigrate> setClientes = new HashSet<>();
	static Set<Venda> setVendas = new HashSet<>();
	static Set<Fornecedor> setFornecedores = new HashSet<>();
	@Autowired public static ClienteService clienteDAO;
	@Autowired public static VendaDAO vendaDAO;
	/*CHAMA TODOS OS IMPORTS*/
	public static void main(String[] args) throws Exception {
		iniciar();
	}
	
	public static void iniciar(){
		System.out.println("Iniciando conversão...");
		
		for (Imports arquivo : Imports.values()) {
			System.out.println("\n\nLendo " +arquivo);
			
			try {
				switch (arquivo) {
				case CLIENTE:
					setClientes.addAll(new ClienteMigrate().lerDBF(arquivo.getUrl()));
					break;
				case VENDA:
					setVendas.addAll(new Venda().lerDBF(arquivo.getUrl()));
					break;
				case FORNECEDOR:
					setFornecedores.addAll(new Fornecedor().lerDBF(arquivo.getUrl()));
					break;
				default:
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			/*System.out.println("Tamanho SetCliente: "+setClientes.size());
			Set<ClienteMigrate> clientesSave = new HashSet<>(clienteDAO.saveCollections(setClientes));
//			Set<Venda> vendaSave = new HashSet<>(vendaDAO.saveCollections(ligarVendaCliente(clientesSave,setVendas)));
			System.out.println("Tamanho Cliente: "+clientesSave.size());
			Set<Venda> vendaSave = new HashSet<>(ligarVendaCliente(clientesSave,setVendas));
			System.out.println("Tamanho Venda: " +vendaSave.size());*/
			Endereco endereco = new Endereco();
			endereco.setCep("123");
			endereco.setCidade("Joao pessoa");
			List<Endereco> end = new ArrayList<Endereco>();
			end.add(endereco);
			Cliente cliente = new Cliente("Mateus","123","12345","88888", "mat@mat.com",
					"observacao",new Date(),new Date(),"Miguel","Maria","Joao");
			clienteDAO.save(cliente);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*for (Cliente cliente : setClientes) {
			System.out.println("\n"+cliente);
		}*/
/*		for (Fornecedor fornecedor : setFornecedores) {
			System.out.println("\n"+fornecedor);
		}*/
	}
	
	private static Set<Venda> ligarVendaCliente(Set<ClienteMigrate> clientes, Set<Venda> vendas) {
		for (Venda venda : vendas) {
			for (ClienteMigrate cliente : clientes) {
				if(venda.getClienteMigrate().getNome().equals(cliente.getNome())) {
					venda.getClienteMigrate().setId(cliente.getId());
					break;
				}
			}
		}
		return vendas;
	}

	public void validaTam(int i, Set<?> list, String path) throws Exception {
		System.out.println("["+path+"] Java Size: "+i+ " Set Size: "+list.size());
		
		if(i != list.size()) {
			throw new Exception("Tamanho diferente");
		}
	}
}