package br.com.migrate.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.celiamagazine.dao.AbstractDAO;
import br.com.migrate.model.ClienteMigrate;

@Repository
@Transactional
public class ClienteDAO extends AbstractDAO<ClienteMigrate>{

	
}