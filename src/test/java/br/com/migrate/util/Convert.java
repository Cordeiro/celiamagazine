package br.com.migrate.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Convert {
	public Date formatData(String data) throws Exception {
		int fim = data.lastIndexOf("T")+2;
		String ano = "";
		String dia = data.substring(8, 10);
		String mes = data.substring(4, 7);
		try {

			if (fim == 24)
				ano = data.substring(fim, 28);
			else if (fim == 25)
				ano = data.substring(fim, 29);
			else {
				System.out.println("FIM INVALIDO");
				throw new Exception();
			}

			if (dia.length() != 2) {
				System.out.println("Valor do dia errado: " + dia);
				return null;
			}
			if (mes.length() != 3) {
				System.out.println("Valor do mês errado: " + mes);
				return null;
			}
			if (ano.length() != 4 || Integer.parseInt(ano) < 1900 || Integer.parseInt(ano) > Calendar.getInstance().getWeekYear()) {
				System.out.println("Valor de ano errado: " + ano);
				return null;
			}
			int mesValue = Mes.valueOf(mes).ordinal()+1;
			if(mesValue < 10)
				mes = "0" + mesValue;
			else mes = String.valueOf(mesValue);

			//System.out.println(dia+"/"+mes+"/"+ano);
		/*} catch(IllegalArgumentException e) {
			System.out.println("Erro mes: " +mes);
			e.printStackTrace();
			return;*/
		} catch (StringIndexOutOfBoundsException e) {
			return null;
		}
		
		return new SimpleDateFormat("dd/MM/yyyy").parse(dia+"/"+mes+"/"+ano);
	}
	
}