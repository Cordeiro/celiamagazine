package br.com.migrate.util;

public enum Imports {
	CLIENTE("./dbf/CLIENTES.DBF"),
	VENDA("./dbf/RECEBER.DBF"),
	FORNECEDOR("./dbf/FORNECED.DBF"),
	CAIXA("./dbf/CAIXA.DBF");
	
	private String url;

	private Imports(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}