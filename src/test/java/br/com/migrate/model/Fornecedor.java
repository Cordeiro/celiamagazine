package br.com.migrate.model;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import br.com.migrate.SuiteImportAll;
import br.com.migrate.read.DBFReader;
import br.com.migrate.read.JDBFException;

public class Fornecedor {
	
	private String nome;
	private String contato;
	private String ie;
	private Endereco endereco;
	private String fone;
	private String email;
	private String observacao;
	
	DBFReader dbfReader;
	
	public Fornecedor() {
		this.endereco = new Endereco();
	}
	
	public Set<Fornecedor> lerDBF(String path) throws Exception{
		Set<Fornecedor> list = new HashSet<Fornecedor>();
		int size = 0;
		try {
			dbfReader = new DBFReader(path);
			/*for (int i = 0; i < dbfReader.getFieldCount(); i++)
				System.out.print(dbfReader.getField(i).getName() + "-"); // NOME DOS ATRIBUTOS*/ 
			
			for (size = 0; dbfReader.hasNextRecord(); size++) 
				list.add(getDBF(dbfReader));
			
/*			for (Fornecedor fornecedor : list)
				System.out.println("\n"+fornecedor);*/
			
			new SuiteImportAll().validaTam(size, list, path);
		}catch (JDBFException e) {
			e.printStackTrace();
			System.out.println("Erro! Arquivo '" + path + "'");
		}
		
		return list;
	}
	
	private Fornecedor getDBF(DBFReader dbf) throws Exception{
		Object aobj[];
		Fornecedor fornecedor = new Fornecedor();
		try {
			aobj = dbf.nextRecord(Charset.forName("UTF-8"));
			for (int i = 0;	i< aobj.length; i++) {
				switch (i) {
				case 0: // NOME
					fornecedor.setNome(aobj[i].toString());
					break;
				case 1: // CONTATO
					fornecedor.setContato(aobj[i].toString());
					break;
				case 2: // IE
					fornecedor.setIe(aobj[i].toString());
					break;
				case 3: // IM
					break;
				case 4: // CGC
					break;
				case 5: // ENDERE
					fornecedor.getEndereco().setRua(aobj[i].toString());
					break;
				case 6: // COMPLE
					fornecedor.getEndereco().setComplemento(aobj[i].toString());
					break;
				case 7: // CIDADE
					fornecedor.getEndereco().setCidade(aobj[i].toString());
					break;
				case 8: // ESTADO
					fornecedor.getEndereco().setEstado(aobj[i].toString());
					break;
				case 9: // CEP
					fornecedor.getEndereco().setCep(aobj[i].toString());
					break;
				case 10: // FONE
					fornecedor.setFone(aobj[i].toString());
					break;
				case 11: // FAX
					break;
				case 12: // CELULAR
					break;
				case 13: // EMAIL
					fornecedor.setEmail(aobj[i].toString());
					break;
				case 14: // OBS
					fornecedor.setObservacao(aobj[i].toString());
					break;
				case 15: // ATIVO
					break;
				default:
					throw new Exception("ERRO! Limite ultrapassado");
				}
			}
		} catch (NullPointerException e) {
		} catch (JDBFException e) {
			e.printStackTrace();
		}
		
		return fornecedor;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getContato() {
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public String getIe() {
		return ie;
	}
	public void setIe(String ie) {
		this.ie = ie;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public String getFone() {
		return fone;
	}
	public void setFone(String fone) {
		this.fone = fone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public String toString() {
		return "Fornecedor [nome=" + nome + ", contato=" + contato + ", ie=" + ie + ", endereco=" + endereco + ", fone="
				+ fone + ", email=" + email + ", observacao=" + observacao + "]";
	}
}