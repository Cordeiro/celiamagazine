package br.com.migrate.model;

import javax.persistence.Entity;

import br.com.celiamagazine.model.AbstractEntity;

@Entity
public class Endereco extends AbstractEntity{
	
	private String rua;
	private String complemento;
	private String cidade;
	private String estado;
	private String cep;
	
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public String toString() {
		return "Endereco [rua=" + rua + "\ncomplemento=" + complemento + "\ncidade=" + cidade + "\nestado=" + estado
				+ "\ncep=" + cep + "]";
	}
}
