package br.com.migrate.model;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;

import br.com.celiamagazine.model.AbstractEntity;
import br.com.migrate.SuiteImportAll;
import br.com.migrate.read.DBFReader;
import br.com.migrate.read.JDBFException;
import br.com.migrate.util.Convert;

/*A VENDA DEVE SER ATRELADA AO CLIENTE ATRAVES DO ID E NÃO DO NOME*/
@Entity
public class Venda extends AbstractEntity{
	private String historico;
	private Date dataEmissao;
	private Date dataVencimento;
	private Date dataRecebimento;
	private String valorVenda;
	private String valorRecebido;
	private String valorJuros;
	private ClienteMigrate cliente;
	
	DBFReader dbfReader;
	
	public Venda(){
		this.cliente = new ClienteMigrate();
	}
	
	public Set<Venda> lerDBF(String path) throws Exception{
		Set<Venda> list = new HashSet<Venda>();
		int size = 0;
		try {
			dbfReader = new DBFReader(path);
//			for (int i = 0; i < dbfReader.getFieldCount(); i++)
//				System.out.print(dbfReader.getField(i).getName() + "-"); // NOME DOS ATRIBUTOS 
			
			for (size = 0; dbfReader.hasNextRecord(); size++) 
				list.add(getDBF(dbfReader));
			
/*			for (Venda venda : list)
				System.out.println("\n"+venda);*/
			
			new SuiteImportAll().validaTam(size, list, path);
		}catch (JDBFException e) {
			e.printStackTrace();
			System.out.println("Erro! Arquivo '" + path + "'");
		}
		
		return list;
	}
	
	private Venda getDBF(DBFReader dbf) throws Exception{
		Object aobj[];
		Venda venda = new Venda();
		try {
			aobj = dbf.nextRecord(Charset.forName("UTF-8"));
			for (int i = 0;	i< aobj.length; i++) {
				switch (i) {
				case 0: // HISTORICO
					venda.setHistorico(aobj[i].toString());
					break;
				case 1: // PORTADOR
					break;
				case 2: // DOCUMENTO
					break;
				case 3: // NOME
					venda.getClienteMigrate().setNome(aobj[i].toString());
					break;
				case 4:	// EMISSAO
					venda.setDataEmissao(aobj[i].toString());
					break;
				case 5:	// VENCIMENTO
					venda.setDataVencimento(aobj[i].toString());
					break;
				case 6:	// VALOR_DUPL
					venda.setValorVenda(aobj[i].toString());
					break;
				case 7:	// RECEBIMENT
					venda.setDataRecebimento(aobj[i].toString());
					break;
				case 8:	// VALOR_RECE
					venda.setValorRecebido(aobj[i].toString());
					break;
				case 9:	// VALOR_JURO
					venda.setValorJuros(aobj[i].toString());
					break;
				case 10:// ATIVO
					break;
				case 11:// CONTA
					break;
				case 12:// NOSSONUM
					break;
				default:
					throw new Exception("ERRO! Limite ultrapassado");
				}
			}
		} catch (NullPointerException e) {
		} catch (JDBFException e) {
			e.printStackTrace();
		}
		
		return venda;
	}

	public String getHistorico() {
		return historico;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public Date getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) throws Exception {
		this.dataEmissao = new Convert().formatData(dataEmissao);
	}
	public Date getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(String dataVencimento) throws Exception {
		this.dataVencimento = new Convert().formatData(dataVencimento);
	}
	public Date getDataRecebimento() {
		return dataRecebimento;
	}
	public void setDataRecebimento(String dataRecebimento) throws Exception {
		this.dataRecebimento = new Convert().formatData(dataRecebimento);
	}
	public String getValorVenda() {
		return valorVenda;
	}
	public void setValorVenda(String valorVenda) {
		this.valorVenda = valorVenda;
	}
	public String getValorRecebido() {
		return valorRecebido;
	}
	public void setValorRecebido(String valorRecebido) {
		this.valorRecebido = valorRecebido;
	}
	public String getValorJuros() {
		return valorJuros;
	}
	public void setValorJuros(String valorJuros) {
		this.valorJuros = valorJuros;
	}
	public ClienteMigrate getClienteMigrate() {
		return cliente;
	}
	public void setClienteMigrate(ClienteMigrate cliente) {
		this.cliente = cliente;
	}
	@Override
	public String toString() {
		return "Venda [historico=" + historico + ", dataEmissao=" + dataEmissao + ", dataVencimento=" + dataVencimento
				+ ", dataRecebimento=" + dataRecebimento + ", valorVenda=" + valorVenda + ", valorRecebido="
				+ valorRecebido + ", valorJuros=" + valorJuros + ", cliente=" + cliente.getNome() + "]";
	}
}