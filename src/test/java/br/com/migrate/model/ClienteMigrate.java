package br.com.migrate.model;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.celiamagazine.model.AbstractEntity;
import br.com.migrate.SuiteImportAll;
import br.com.migrate.read.DBFReader;
import br.com.migrate.read.JDBFException;
import br.com.migrate.util.Convert;

@Entity
@Table(name = "cliente")
public class ClienteMigrate extends AbstractEntity {

	private String nome;
	private String contato;
	private String rg;
	private String cpf;
	private List<Endereco> enderecos;
	@Column(name = "telefone")
	private String fone;
	private String email;
	private String observacao;
	private String pai;
	private String mae;
	private Date dtNascimento;
	private Date dtCadastro;

	DBFReader dbfReader;

	public ClienteMigrate() {
		enderecos = new ArrayList<Endereco>();
	}
	
	

	public ClienteMigrate(String nome, String contato, String rg, String cpf, List<Endereco> enderecos, String fone,
			String email, String observacao, String pai, String mae, Date dtNascimento, Date dtCadastro) {
		super();
		this.nome = nome;
		this.contato = contato;
		this.rg = rg;
		this.cpf = cpf;
		this.enderecos = enderecos;
		this.fone = fone;
		this.email = email;
		this.observacao = observacao;
		this.pai = pai;
		this.mae = mae;
		this.dtNascimento = dtNascimento;
		this.dtCadastro = dtCadastro;
	}



	public void setDtCadastro(String dtCadastro) throws Exception {
		this.dtCadastro = new Convert().formatData(dtCadastro);
	}

	public Set<ClienteMigrate> lerDBF(String path) throws Exception {
		Set<ClienteMigrate> list = new HashSet<ClienteMigrate>();
		int size = 0;
		try {
			dbfReader = new DBFReader(path);
			/*
			 * for (int i = 0; i < dbfReader.getFieldCount(); i++)
			 * System.out.print(dbfReader.getField(i).getName() + "-"); // NOME DOS
			 * ATRIBUTOS
			 */

			for (size = 0; dbfReader.hasNextRecord(); size++)
				list.add(getDBF(dbfReader));

			new SuiteImportAll().validaTam(size, list, path);
			/*
			 * for (Cliente cliente : list) { System.out.println("\n"+cliente); }
			 */
		} catch (JDBFException e) {
			e.printStackTrace();
			System.out.println("Erro! Arquivo '" + path + "'");
		}

		return list;
	}

	public static ClienteMigrate getDBF(DBFReader dbf) throws Exception {
		Object aobj[];
		ClienteMigrate cliente = new ClienteMigrate();
		Endereco end = new Endereco();
		try {
			aobj = dbf.nextRecord(Charset.forName("UTF-8"));
			for (int i = 0; i < aobj.length; i++) {
				switch (i) {
				case 0: // NOME
					cliente.setNome(aobj[i].toString());
					break;
				case 1: // CONTATO
					cliente.setContato(aobj[i].toString());
					break;
				case 2: // RG(IE)
					cliente.setRg(aobj[i].toString());
					break;
				case 3: // (IM)
					break;
				case 4: // (CGC)
					cliente.setCpf(aobj[i].toString());
					break;
				case 5: // RUA
					end.setRua(aobj[i].toString());
					break;
				case 6: // COMPLEMENTO
					end.setComplemento(aobj[i].toString());
					break;
				case 7: // CIDADE
					end.setCidade(aobj[i].toString());
					break;
				case 8: // ESTADO
					end.setEstado(aobj[i].toString());
					break;
				case 9: // CEP
					end.setCep(aobj[i].toString());
					break;
				case 10: // FONE
					cliente.setFone(aobj[i].toString());
					break;
				case 11: // FAX
					break;
				case 12: // EMAIL
					cliente.setEmail(aobj[i].toString());
					break;
				case 13: // OBSERVAÇÃO
					cliente.setObservacao(aobj[i].toString());
					break;
				case 14: // CELULAR
					break;
				case 15: // CREDITO
					break;
				case 16: // PAI
					cliente.setPai(aobj[i].toString());
					break;
				case 17: // MAE
					cliente.setMae(aobj[i].toString());
					break;
				case 18: // CONJUGE
					break;
				case 19: // NATURAL
					break;
				case 20: // PROFISSAO
					break;
				case 21: // CONVENIO
					break;
				case 22: // DATA NASCIMENTO
					cliente.setDtNascimento(aobj[i].toString());
					break;
				case 23: // CADASTRO DATA
					cliente.setDtCadastro(aobj[i].toString());
					break;
				case 24: // ULTIMA COMPRA DATA
					break;
				case 25: // PROXIMA DATA
					break;
				case 26: // CUSTO
					break;
				case 27: // COMPRA
					break;
				case 28: // ATIVO
					break;
				case 29: // MOSTRAR
					break;
				default:
					throw new Exception("ERRO! Limite ultrapassado");
				}
			}
		} catch (NullPointerException e) {
		} catch (JDBFException e) {
			e.printStackTrace();
		}
		cliente.getEndereco().add(end);
		return cliente;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Endereco> getEndereco() {
		return enderecos;
	}

	public void setEndereco(List<Endereco> endereco) {
		this.enderecos = endereco;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public void setDtNascimento(String dtNascimento) throws Exception {
		/*
		 * List<String> asList =
		 * Arrays.asList("Tue May 10 00:00:00 BRT 964","Nov 24 00:00:00 BRT 982",
		 * "Fri Nov 24 00:00:00 BRT 982","Tue Apr 20 00:00:00 BRT 986"
		 * ,"Mon Sep 22 00:00:00 BRT 189");
		 */
		this.dtNascimento = new Convert().formatData(dtNascimento);
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public String toString() {
		return "Cliente [nome=" + nome + "\ncontato=" + contato + "\nrg=" + rg + "\ncpf=" + cpf + "\nendereco="
				+ enderecos + "\nfone=" + fone + "\nemail=" + email + "\nobservacao=" + observacao + "\npai=" + pai
				+ "\nmae=" + mae + "\ndtNascimento=" + dtNascimento + "\ndtCadastro=" + dtCadastro + "]";
	}

}